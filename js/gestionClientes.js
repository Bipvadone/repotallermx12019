var clientesObtenidos;



function getClients()
{
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request =new XMLHttpRequest();
  request.onreadystatechange=function()
  {
    if(this.readyState== 4 && this.status==200)
    {
      clientesObtenidos=request.responseText;
      ClientesObtenidos();

    }
  }
  request.open("GET",url,true);
  request.send();
};


function ClientesObtenidos()
{
  var JSONClientes =JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla=document.getElementById("tablaClientes");
  var tabla= document.createElement("table");
  var tbody= document.createElement("tbody");
tabla.classList.add("table");
tabla.classList.add("table-striped");




for (var i=0;i<JSONClientes.value.length;i++)
{
//  console.log(JSONProductos.value[i].ProductName);

var nuevaFila = document.createElement("tr");

var columnaNombre = document.createElement("td");
columnaNombre.innerText=JSONClientes.value[i].ContactName;

var columnaCity = document.createElement("td");
columnaCity.innerText=JSONClientes.value[i].City;

var columnaCountry = document.createElement("td");
 
var img = document.createElement("img");
 
img.setAttribute("src",getFlags_(JSONClientes.value[i].Country));

img.classList.add("flag");

nuevaFila.appendChild(columnaNombre);
nuevaFila.appendChild(columnaCity);
nuevaFila.appendChild(img);

tbody.appendChild(nuevaFila);

}

tabla.appendChild(tbody);

divTabla.appendChild(tabla);


};


///flags
var datos;
function getFlags(country)
{

  var url="https://restcountries.eu/rest/v2/name/"+country+"?fullText=true";
  var request =new XMLHttpRequest();
  request.onreadystatechange=function()
  {
    if(this.readyState== 4 && this.status==200)
    {
      datos=request.responseText;
    }
  }
  request.open("GET",url,false);
  request.send();

}

function getFlags_(country)
{
  getFlags(country);
 
  var JSONBandera =JSON.parse(datos);
  console.log(JSONBandera[0].flag);
 
  return JSONBandera[0].flag;
}

